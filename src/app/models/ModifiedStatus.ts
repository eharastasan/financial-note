export enum ModificationStatus{
    deleted = -1,
    untouched = 0,
    modified = 1,
    created = 2,
}