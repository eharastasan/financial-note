import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { GuestPage } from '../pages/guest/guest.page';
import { HomePage } from '../pages/home/home.page';
import { DashboardPage } from '../pages/home/dashboard/dashboard.page';
import { ReportsPage } from '../pages/home/reports/reports.page';
import { AuthGuard } from '../services/auth-guard';

const routes: Routes = [
    {
        path: 'guest',
        component: GuestPage
    },
    {
        canActivate : [AuthGuard],
        path: 'home',
        component: HomePage,
        children: [
            {
                path: 'dashboard',
                component: DashboardPage
            },
            {
                path: 'reports',
                component: ReportsPage
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'reports'
            }
        ]
    },
    {
        path: '**',
        redirectTo: 'guest'
    }
];
@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
