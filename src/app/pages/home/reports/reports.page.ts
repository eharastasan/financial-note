import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import { Category } from 'src/app/models/Category';
import { NotifierService } from 'src/app/services/notifier.service';

@Component({
    selector: 'app-reports',
    templateUrl: './reports.page.html',
    styleUrls: ['./reports.page.scss'],
})
export class ReportsPage implements OnInit {

    public reports : Array<Category>;

    constructor(
        private databaseService: DatabaseService,
        private notifierService : NotifierService
    ) { }

    ngOnInit() {
        this.reports = this.databaseService.financialNotes;
        this.databaseService.onDataUpdatedInBBDD.subscribe((financialNotes) => this.reports = financialNotes);
        this.notifierService.updateRouteTitle("REPORTS.TITLE","list_alt");
    }

}
