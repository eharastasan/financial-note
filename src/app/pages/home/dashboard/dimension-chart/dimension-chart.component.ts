import { Component, OnInit, Input } from '@angular/core';
import { Category } from 'src/app/models/Category';

@Component({
    selector: 'app-dimension-chart',
    templateUrl: './dimension-chart.component.html',
    styleUrls: ['./dimension-chart.component.scss'],
})
export class DimensionChartComponent implements OnInit {

    @Input() financialNotes: Category[];

    constructor() { }

    ngOnInit() {
        console.log(this.financialNotes);
    }

}
