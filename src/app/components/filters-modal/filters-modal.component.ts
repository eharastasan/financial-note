import { Component, OnInit, ViewChild, Inject, OnDestroy, AfterViewInit } from '@angular/core';
import { FiltersService } from 'src/app/services/filters.service';
import { DatabaseService } from 'src/app/services/database.service';
import { Category } from 'src/app/models/Category';
import { MatSelectionList, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDialogRef, MatExpansionPanel } from '@angular/material';
import { Entry } from 'src/app/models/Entry';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { FormGroup, FormBuilder } from '@angular/forms';
import _ from "lodash";
import { IFiltersModel } from 'src/app/models/IFiltersModel';
import moment from 'moment';
import { NotifierService } from 'src/app/services/notifier.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-filters-modal',
    templateUrl: './filters-modal.component.html',
    styleUrls: ['./filters-modal.component.scss'],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        { provide : MAT_DATE_FORMATS,
        useValue : {
            parse : {
                dateInput : "YYYY-MM-DD"
            },
            display: {
                dateInput: 'YYYY-MM-DD'
            }
        }
    }]
})
export class FiltersModalComponent implements OnInit,AfterViewInit, OnDestroy {

    @ViewChild('catergoiesListElement') catergoiesListElement: MatSelectionList;
    @ViewChild('entriesListElement') entriesListElement: MatSelectionList;
    @ViewChild('expansionCategoryPanel') expansionCategoryPanel: MatExpansionPanel;
    @ViewChild('expansionEntryPanel') expansionEntryPanel: MatExpansionPanel;
    public financialNotes : Category[];
    public entries : String[];
    public datesRange: FormGroup;
    private filtredDataSubscription : Subscription;
    public firstDate : string;
    public lastDate : string;
    
    constructor(
        private filtersService  : FiltersService,
        private databaseService : DatabaseService,
        private formBuilder: FormBuilder,
        private notifierService : NotifierService,
        public dialogRef: MatDialogRef<FiltersModalComponent>
    ) { }

    ngOnInit() {
        this.financialNotes = this.databaseService.financialNotes;
        this.entries = this.getEntriesList();
        this.getFirstAndLastDates();
        this.createFormControls();
    }
    
    ngAfterViewInit() {
        this.setSelectedFilters();

    }

    private getEntriesList() : String[]{
        return _.chain(this.databaseService.financialNotes)
            .flatMap((category : Category)=>{
                return category.entries;
            })
            .map((entry : Entry)=>{
                return entry.description;
            })
            .uniq()
            .orderBy()
            .value();
    }

    private getFirstAndLastDates(){
        let uniqDates = _.chain(this.databaseService.financialNotes)
            .flatMap((category : Category)=>{
                return category.entries;
            })
            .map((entry : Entry)=>{
                return entry.date;
            })
            .uniq()
            .orderBy()
            .value();

        let firstDate = moment(_.first(uniqDates),"YYYY-MM-DD");
        let lastDate = moment(_.first(uniqDates),"YYYY-MM-DD");
        _.forEach(uniqDates,(date : string)=>{
            let iterationDate = moment(date,"YYYY-MM-DD");

            if(moment(firstDate,"YYYY-MM-DD").isAfter(iterationDate)){
                firstDate = iterationDate;
            }

            if(moment(lastDate,"YYYY-MM-DD").isBefore(iterationDate)){
                lastDate = iterationDate;
            }
        })
        this.firstDate = firstDate.format("YYYY-MM-DD");
        this.lastDate = lastDate.format("YYYY-MM-DD");
    }

    private createFormControls(){
        let startDate = this.firstDate;
        let endDate = this.lastDate;
        
        if(!_.isUndefined(this.filtersService.filtersModel)){
            startDate = _.isUndefined(this.filtersService.filtersModel.startDate) ?  null : this.filtersService.filtersModel.startDate.format("YYYY-MM-DD");
            endDate = _.isUndefined(this.filtersService.filtersModel.endDate) ?  null : this.filtersService.filtersModel.endDate.format("YYYY-MM-DD");
        }
        this.datesRange = this.formBuilder.group({
            startDate : [startDate,[]],
            endDate : [endDate,[]],
        });
    }

    private setSelectedFilters(){
        if(!_.isUndefined(this.filtersService.filtersModel)){
            this.catergoiesListElement.options.forEach((option)=>{
                let selectedCategory = _.find(this.filtersService.filtersModel.categories,(category) => category.id == option.value.id);
                if(selectedCategory){
                    option.selected = true;
                }
            });

            this.entriesListElement.options.forEach((option)=>{
                let selectedEntry = _.find(this.filtersService.filtersModel.entries,(entry) => entry == option.value);
                if(selectedEntry){
                    option.selected = true;
                }
            });
        }
    }

    public onClickClearDatesValue(datePicker : string){
        this.datesRange.controls[datePicker].reset();
    }

    public onClickClearListValue(list : string, event : Event){
        event.stopPropagation();
        this[list].deselectAll();
    }

    public onClickApplyFilters(){
        let filtersModel : IFiltersModel = {
            categories : _.map(this.catergoiesListElement.selectedOptions.selected,(selection)=>{ return selection.value}),
            entries : _.map(this.entriesListElement.selectedOptions.selected,(selection)=>{ return selection.value}),
            startDate : moment(this.datesRange.value.startDate,"YYYY-MM-DD"),
            endDate : moment(this.datesRange.value.endDate,"YYYY-MM-DD"),
        };

        this.filtredDataSubscription = this.filtersService
            .filtredData
            .subscribe(()=>{
                this.notifierService.success("MODALS.FILTERS.FILTERSAPPLIED");
                this.dialogRef.close();
            },()=>{
                this.notifierService.error("MODALS.FILTERS.FAILEDTOAPPLYFILTERS");
            });
        this.filtersService.applyFilters(filtersModel);
    }

    public onClickCloseModal(){
        this.dialogRef.close();
    }
    
    ngOnDestroy(){
        if(this.filtredDataSubscription) this.filtredDataSubscription.unsubscribe();
    }
}
