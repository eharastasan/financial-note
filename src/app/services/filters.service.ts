import { Injectable } from '@angular/core';
import { IFiltersModel } from '../models/IFiltersModel';
import { Subject, Observable } from 'rxjs';
import { Category } from '../models/Category';
import { DatabaseService } from './database.service';
import { Entry } from '../models/Entry';
import _ from "lodash";
import moment from "moment";

@Injectable({
    providedIn: 'root'
})
export class FiltersService {

    private setFilters: Subject<{ filteredData : Array<Category>, filtersModel : IFiltersModel}> = new Subject<{ filteredData : Array<Category>, filtersModel : IFiltersModel}>();
    private filteredData : Category[];
    public filtersModel : IFiltersModel = {};
    public filtredData: Observable<{ filteredData : Array<Category>, filtersModel : IFiltersModel}> = this.setFilters.asObservable();

    constructor(
        private databaseService: DatabaseService
    ) { }

    public applyFilters(filtersModel?: IFiltersModel) {
        this.filtersModel = filtersModel;
        if(this.filtersModel){
            this.filterByCategory();
            this.filterByEntry();
            this.filterByDate();
            this.setFilters.next({filteredData : this.filteredData, filtersModel : filtersModel});
        }
        else{
            this.setFilters.next({filteredData : this.databaseService.financialNotes, filtersModel : filtersModel});
        }
    }

    private filterByCategory(){
        if(this.filtersModel.categories.length > 0){
            this.filteredData = _.cloneDeep(this.filtersModel.categories);
        }
        else{
            this.filteredData = _.cloneDeep(this.databaseService.financialNotes);
        }
    }

    private filterByEntry(){
        if(this.filtersModel.entries.length > 0){
            this.filteredData = _.map(this.filteredData,(category : Category)=>{
                category.entries = _.filter(category.entries,(entry : Entry)=>{
                    return _.includes(this.filtersModel.entries,entry.description);
                });
                return category;
            });
        }
    }

    private filterByDate(){
        if(this.filtersModel.startDate.isValid() || this.filtersModel.endDate.isValid()){
            this.filteredData = _.map(this.filteredData,(category : Category)=>{

                category.entries = _.filter(category.entries,(entry : Entry)=>{

                    let entryDate = moment(entry.date,"YYYY-MM-DD");
                    if(this.filtersModel.startDate.isValid() && this.filtersModel.endDate.isValid()){
                        return entryDate.isBetween(this.filtersModel.startDate,this.filtersModel.endDate,null,"[]");
                    }
                    else if(this.filtersModel.startDate.isValid()){
                        return entryDate.isSameOrAfter(this.filtersModel.startDate);
                    }
                    else if(this.filtersModel.endDate.isValid()){
                        return entryDate.isSameOrBefore(this.filtersModel.endDate);
                    }
                    return true;
                });
                return category;
            });
        }
    }
}
