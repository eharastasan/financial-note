import { Injectable, OnDestroy } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { Category } from '../models/Category';
import { Entry } from '../models/Entry';
import { Observable, Subscription, Subject } from 'rxjs';
import { ModificationStatus } from '../models/ModifiedStatus';
import * as _ from 'lodash';

@Injectable()
export class DatabaseService implements OnDestroy{

    private _financialNotesCopy         : Array<Category>;
    private dbUrl                       : string;
    private user                        : any;
    private getDataCollectionsSubscribe : Subscription;
    public updateDDBBSubject            : Subject<Array<Category>> = new Subject<Array<Category>>();
    public onDataUpdatedInBBDD          : Observable<Array<Category>> = this.updateDDBBSubject.asObservable();
    public financialNotes               : Array<Category>;
    
    constructor(
        private fireAuth: AngularFireAuth,
        private db: AngularFireDatabase
    ) { }

    public getParsedDataCollections() : Observable<Array<Category>>{
        return new Observable<Array<Category>>((subscriber)=>{

            this.getDataCollectionsSubscribe = this.getDataCollections()
                .subscribe(( data : Object ) => {
                    let categories : Array<Category> = [];

                    for (const key in data) {
                        const unparsedCategory = data[key];
                        let category : Category = Object.assign(new Category,unparsedCategory);
                        let entries : Array<Entry> = [];

                        for (const _key in category.entries) {
                            const unparsedEntry = category.entries[_key];
                            unparsedEntry.id = _key;
                            entries.push(Object.assign(new Entry,unparsedEntry));
                        }

                        category.entries = entries;
                        categories.push(category);
                    }
                    this.financialNotes = categories;
                    this._financialNotesCopy = Object.assign(this.financialNotes);
                    subscriber.next(this.financialNotes);
                    this.updateDDBBSubject.next(this.financialNotes);
                });
        })
    }

    public saveChanges() : Promise<any>{
        return new Promise((resolve)=>{

            this.iterateOverDataCollectionToModifyInBBDD()
                .then(()=>{
                    this.financialNotes = this.financialNotes.filter((category: Category) => {
                        category.entries = category.entries.filter((entry : Entry)=>{
                            return entry.modificationStatus !== ModificationStatus.deleted;
                        });
                        return category.modificationStatus !== ModificationStatus.deleted;
                    });

                    this.financialNotes.forEach((category : Category) =>{
                        category.modificationStatus = ModificationStatus.untouched;

                        category.entries.forEach((entry : Entry)=>{
                            entry.modificationStatus = ModificationStatus.untouched;
                        });
                    });
            
                    this._financialNotesCopy = Object.assign(this.financialNotes);
                    this.updateDDBBSubject.next(this.financialNotes);
                    resolve();
                })
        });
    }

    public resetData(){
        this.financialNotes = Object.assign(this._financialNotesCopy);
    }

    private getDataCollections() : Observable<any>{
        this.user = this.fireAuth.auth.currentUser;
        this.dbUrl = "/" + this.user.uid;
        this.db.object("/" + this.user.uid + "/financialNotes")
            .query
            .once("value")
            .then(data => {
                if (data.val() == null) {
                    this.setInitCollections();
                }
            });

        return this.db.object("/" + this.user.uid + "/financialNotes").valueChanges();
    }

    private setInitCollections() {
        this.db.object(this.dbUrl + "/financialNotes").set({});
        this.db.object(this.dbUrl + "/userEmail").set(this.user.email);
        this.db.object(this.dbUrl + "/userId").set(this.user.uid);
        this.db.object(this.dbUrl + "/userName").set(this.user.displayName);
    }

    private iterateOverDataCollectionToModifyInBBDD() : Promise<any>{
        let promisesArray = [];
        this.financialNotes.forEach((category: Category, index : number) => {
            promisesArray.push(this.modifyDataInBBDD(category));
            
            category.entries.forEach((entry : Entry, index : number) => {
                promisesArray.push(this.modifyDataInBBDD(entry));
            });
        });
        return promisesArray.reduce((promise, accumulator) => promise.then(accumulator), Promise.resolve());
    }

    private modifyDataInBBDD(object :  Category | Entry) : Promise<any>{
        switch (object.modificationStatus) {
            case ModificationStatus.created:
                if(object instanceof Category) return this.createCategory(object);
                else if(object instanceof Entry) return this.createEntry(object);
                break;
            case ModificationStatus.modified:
                if(object instanceof Category) return this.editCategory(object);
                else if(object instanceof Entry) return this.editEntry(object);
                break;
            case ModificationStatus.deleted:
                if(object instanceof Category) return this.deleteCategory(object);
                else if(object instanceof Entry) return this.deleteEntry(object);
                break;
        }
    }

    public createCategory(category: Category) : Promise<any>{
        let lastNoteIndex = _.last(this.financialNotes).listIndex;
        category.listIndex = (lastNoteIndex + 1);
        return this.db.list(this.dbUrl + "/financialNotes/").push({
            icon: category.icon,
            title: category.title,
            color: category.color,
            listIndex : category.listIndex
        });
    }

    public deleteCategory(category: Category) : Promise<any>{
        return this.db.list(this.dbUrl + "/financialNotes/" + category.id).remove();
    }

    public editCategory(category: Category) : Promise<any>{
        return this.db.list(this.dbUrl + "/financialNotes/").update(category.id, {
            icon: category.icon,
            title: category.title,
            color: category.color
        });
    }

    public createEntry(entry: Entry) : Promise<any>{
        delete entry.modificationStatus;
        return this.db.list(this.dbUrl + "/financialNotes/" + entry.categoryId + "/entries/").push(entry);
    }

    public deleteEntry(entry: Entry) : Promise<any>{
        return this.db.list(this.dbUrl + "/financialNotes/" + entry.categoryId + "/entries/" + entry.id).remove();
    }

    public editEntry(entry: Entry) : Promise<any>{
        return this.db.list(this.dbUrl + "/financialNotes/" + entry.categoryId + "/entries/").update(entry.id, entry);
    }

    ngOnDestroy(){
        if(this.getDataCollectionsSubscribe !== undefined){
            this.getDataCollectionsSubscribe.unsubscribe();
        }
    }
}
