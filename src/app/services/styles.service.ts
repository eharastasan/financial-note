import { Injectable } from '@angular/core';
import * as colorsList from './../../assets/colors/colors.json';
import * as iconsList from '../../assets/icons/icons.json';

@Injectable({
    providedIn: 'root'
})
export class StylesService {

    public colorsList: Array<{name : String, value : String}> = colorsList.colors;
    public iconsList: Array<{name : String, value : String}> = iconsList.icons;

    constructor() { }
}